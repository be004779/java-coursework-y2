package Drone;

public class ConsoleCanvas {
    public char[][] canvas;
    public int canX;
    public int canY;

    /** Code above are pre-defined variables. char[][] canvas is the 2D array for the canvas coordinates. canX is the
    * X coordinate and canY is the coordinate.
     */

    public ConsoleCanvas(int arrX, int arrY){
        canX = arrX;
        canY = arrY;
        canvas = new char[canX][canY];

        for(int i = 0; i < canX; i++) {
            for (int j = 0; j < canY; j++) {
                if (i == 0 || i == canX - 1|| j == 0 || j == canY - 1) {
                    canvas[i][j] = '#';
                } else {
                    canvas[i][j] = ' ';
                }
            }
        }
    }
    /** This is the ConsoleCanvas method, it passes two parameters which are then equalled to the original pre-defined
    * variables, it then passes both the X and Y coordinates through a for loop and a nested for loop to then have a
    * nested if statement to pass through 4 different arguments, this being, if x is equal to 0 or x is equal to canX -1
    * or y is equal to 0 or y is equal to canY - 1 then print out a hashtag to represent the edge of the arena/canvas
    * if not print out a blank space where the drones can be placed.
     */

    public void showIt(int coX, int coY, char d) {
        canvas[coX + 1][coY + 1] = d;
    }
    /** This showIt function defines three parameters, these parameters are used to display the canvas in the console.
    * This is used in the displayDrone function in the Drone file.
     */

    public String toString(){
        String intMessage = "";
        for(int i = 0; i < canX; i++) {
            for (int j = 0; j < canY; j++) {
                intMessage += canvas[i][j] + " ";
                }
            intMessage += "\n";
            }
        return intMessage;
        }
        /** This toString initially sets a new variable to a blank string, a for loop and a nested for loop iterate
        * through the x and y values for which it will add x and y indexes in the canvas function from earlier. This
        * is added to the original string message and then a new line is added at the end of the nested for loop, and
        * the message is returned through the last for loop.
         */
}