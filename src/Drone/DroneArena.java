package Drone;

import java.util.Iterator;
import java.util.Random;
import java.util.ArrayList;

public class DroneArena {
    int arenaX;
    int arenaY;
    ArrayList<Drone> droneList = new ArrayList<Drone>();
    Drone d;
    int valX;
    int valY;

    /** Above are predefined variables, arenaX and arenaY are coordinates of the drone arena, created an arraylist for
    * Drone to store all the Drones and their information. Defined a drone: d. Finally defined valX and valY which
    * act as the drones coordinates in addDrone(). The last two were added later on in the project as an unresolved
    * error appeared and the only way to fix this issue was to pre define the variables.
     */

    public int getX() {
        return arenaX;
    }
    public int getY() {
        return arenaY;
    }
    /** Setters and getters to return the arena coordinates
     */

    public DroneArena(int X, int Y){
        arenaX = X;
        arenaY = Y;
    }
    /** A method to define arenaX and arenaY as X and Y respectively to be used in certain functions such as getDroneAt
    * below.
     */

    public Drone getDroneAt(int X, int Y){
        Drone d = null;
        for(Drone a : droneList){
            if(a.isHere(X, Y)){
                return a = d;
            }else{
                return d;
            }
        }
        return d;
    }
    /** The getDroneAt function equals Drone d as null, another variable called Drone a is then iterated through the
    * droneList array. This is done through a for loop, a nested if statement is then used to check if drone a is
    * at coordinate X and Y, this is done for every drone in the list, if there is a drone at the checked coordinate,
    * then the function will return the drone as d. If not the function will just return Drone d.
     */

    public void addDrone() {
        Random random;
        random = new Random();

        if(droneList.size() < (arenaX * arenaY)){
            do{
                valX = random.nextInt(arenaX);
                valY = random.nextInt(arenaY);
            }while(getDroneAt(valX, valY) != null);
            d = new Drone(valX, valY, Direction.getRandomDir());
            droneList.add(d);
        }
    }
    /** The addDrone function adds a drone to the droneList, it first creates a variable called random through of type
    * Random, this gives the newly added drone a random set of coordinates. The if statement sees if the size of
    * droneList is smaller than the product of the X and Y coordinates of arena. It then uses a do loop to create
    * random X and Y coordinates. Then a while loop to get the drone coordinates using getDroneAt while they are not
    * equal to null to make sure the coordinates are valid, it then adds d as a new Drone with the newly created
    * coordinates, as well as the randomly created direction seen later, it then adds a drone d to droneList
     */

    public String toString() {
        StringBuilder s = new StringBuilder("The arena size is " + arenaX + " x " + arenaY + " and: " + "\n");
        for (Drone d : droneList) {// All the drones in the droneList will be used here and will show all their information
           s.append(d.toString()).append("\n");
        }
        return s.toString();
    }
    /** This toString uses the StringBuilder to create a new string s, this string then uses arenaX and arenaY to show
    * the size of the arena that was created. It then uses a for loop to iterate through the droneList and appends the
    * String s to add the toString for d that we saw previously, and that is also appended to add a new line.
     */

    public void showDrones(ConsoleCanvas c) {
        for (Drone d: droneList) {
            d.displayDrone(c);
        }
    }
    /** This is a simple function to show the drones in droneList on the canvas/arena, it uses a for loop to iterate
    * through the list and then uses the displayDrone function with d to display the drones. It passes a parameter
    * c of type ConsoleCanvas to display the drone in.
     */

    public boolean canMoveHere(int valX, int valY){
        return getDroneAt(valX, valY) == null && valX < arenaX && valY < arenaY && valX >= 0 && valY >= 0;
    }
    /** This is a simple function to see if the drones can move to a certain set of coordinates, it checks if
    * the selected coordinates are empty, hence equal to null, and smaller than the arena dimensions, and bigger than
    * or equal to zero. If all these are true then it returns the function as true as it is a boolean function.
     */

    public void moveAllDrones(){
        for(Drone d : droneList){
            d.tryToMove(this);
        }
    }
    /** This function attempts to move all the drones in the drone list which are iterated through with a for loop
    * it uses the function tryToMove, which is found in Drone
     */
}