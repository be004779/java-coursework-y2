package Drone;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.Scanner;

public class DroneInterface extends Component {
    public int canX;
    public int canY;
    public int counter;
    private Scanner s;
    private DroneArena myArena;

    /**
     * constructor for DroneInterface
     * sets up scanner used for input and the arena
     * then has main loop allowing user to enter commands
     */

    /**
     * X and Y coordinates variable declaration, used for the newArena custom arena function.
     * scanner used for input from user along arena in which drones are shown
     */

    public DroneInterface() throws IOException {
        Scanner s = new Scanner(System.in); // Creating a new scanner variable for user input
        System.out.println("=====================");
        System.out.println("New Arena Dimensions!");
        System.out.println("=====================");

        System.out.println("Please enter the X coordinate");
        try{
            canX = s.nextInt();
        }catch(Exception e){
            System.err.println("Numbers only!");
            s.nextLine();
            canX = s.nextInt();
        }
        System.out.println("Please enter your Y coordinate: ");
        try{
            canY = s.nextInt();
        }catch (Exception e){
            System.err.println("Numbers only!");
            s.nextLine();
            canY = s.nextInt();
        }

        /**
         * The DroneInterface class initially creates a new variable s of type Scanner, this is to detect the user input
         * it then prints out a title that says, and then asks the user to enter the X and Y coordinates, each method
         * has a try and catch statement to make sure the user only enters numbers and no other datatype.
         */
        myArena = new DroneArena(canX, canY); //Creating a new arena based on the user input.
        System.out.println("=================================");
        char ch;
        do {
            System.out.print("Enter\n (A)dd drone\n (I)nformation\n (D)isplay\n (S)ave file\n (L)oad file\n M(O)ve Drones Once\n (M)ove Drones 10 times\n (N)ew Arena\n E(X)it\n  ");
            ch = s.next().charAt(0);
            s.nextLine();
            switch (ch) {
                case 'A': // Adds a new drone to the arena and adds the drone to the counter in the droneCount function
                case 'a':
                    droneCount();
                    break;
                case 'D': // when this is entered, the drones are displayed along with the canvas through the doDisplay function.
                case 'd':
                    doDisplay();
                    break;
                case 'I': //This prints out all the information that the simulator has on the drones that are in the arena.
                case 'i':
                    System.out.print(myArena.toString() + "\n");
                    break;
                case 'M': // This moves the drones 10 times through the animation function.
                case 'm':
                    animation();
                    break;
                case 'N' : // This creates a new Arena that allows the user to add new coordinates.
                case 'n' :
                    s = new Scanner(System.in);
                    System.out.println("=====================");
                    System.out.println("New Arena Dimensions!");
                    System.out.println("=====================");

                    System.out.println("Please enter the X coordinate");
                    try{
                        canX = s.nextInt();
                    }catch(Exception e){
                        System.err.println("Numbers only!");
                        s.nextLine();
                        canX = s.nextInt();
                    }
                    System.out.println("Please enter your Y coordinate: ");
                    try{
                        canY = s.nextInt();
                    }catch (Exception e){
                        System.err.println("Numbers only!");
                        s.nextLine();
                        canY = s.nextInt();
                    }
                    myArena = new DroneArena(canX, canY); //Creating a new arena based on the user input.
                    System.out.println("=================================");
                    break;
                case 'O' : // this asks the simulator to move the drones just once instead of ten times from before, it then asks the simulator display the drones once again.
                case 'o' :
                    myArena.moveAllDrones();
                    doDisplay();
                    break;
                case 'S': // This saves the file, it also has a try catch statement to catch any exceptions while trying to run the function.
                case 's':
                    try {
                        saveFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 'L': // this loads any files that were saved and has the try catch statement again to catch any exceptions.
                case 'l':
                    try {
                        loadFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 'x': // this simply exits the simulator and ends the program.
                case 'X' :
                    ch = 'X';                // when X detected program ends
                    break;
            }
        } while (ch != 'X');                        // test if end
        s.close();                                    // close scanner
    }
    /**
     * Above seen is a very long Switch case statement, it allows the user to input different letters based on what they
     * need out of the drone simulator, each letter is highlighted in a print statement. Each letter has a different
     * job, such as entering 'a' will add a new drone to the simulator.
     */

    public void droneCount() {
        if (counter >= (canX * canY)) {
            System.out.println("Arena is full!");
        } else {
            myArena.addDrone(); // add a new drone to arena
            System.out.println("New Drone Added!");
            counter++;
        }
    }
    /**
     * The droneCount function simply checks if the amount of drones in the arena are equal to or larger than the
     * size of the canvas. If this is true then no drones are added and a message is printed to show the arena is full
     * if not, a new drone is added, a message is printed and the counter is incremented.
     */

    void doDisplay() {
        if (myArena.getX() > 0 && myArena.getY() > 0) {
            ConsoleCanvas canvas = new ConsoleCanvas(myArena.getX() + 2, myArena.getY() + 2);
            myArena.showDrones(canvas);
            System.out.println(canvas.toString());
            if (myArena.droneList.isEmpty()) {
                System.err.println("\nNo drones have been added!");
            }
        }
    }
    /**
     * This function works to display the drones and the arena. It runs an if statement which checks if the arena
     * coordinates are bigger than zero, it then creates a new canvas to print, it then gets the coordinates of the arena
     * and adds 2 to each value, this is to make sure that the canvas and the arena are separate, this makes sure that
     * the drones are only printed within the arena and not on or outside the border. It then asks the function to
     * print the drones and the toString function from canvas. It then runs another if statement to print a message
     * when there are no drones in the list.
     */

    public void animation() {
        int counter = 10;
        for (int i = 0; i < counter; i++) {
            myArena.moveAllDrones();
            doDisplay();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(myArena.toString());
        }
        System.out.println("===========================================");
        System.out.println("   The final display and its information:");
        System.out.println("===========================================");
    }
    /**
     * This function uses the moveAllDrones function to move the drones 10 times in one execution, it starts the counter
     * at 10, and uses a for loop to iterate through each movement, after each movement it displays the arena with the new
     * positions. It then uses a try catch statement to delay the execution by 200 milliseconds for each movement, this is to
     * show that the arena is being printed. It then prints out all the information about the drones positions and directions.
     * and on the final movement, it shows a message.
     */

    public void saveFile() throws IOException {
        String currentDirectory = System.getProperty("user.dir");
        JFileChooser chooser = new JFileChooser(currentDirectory);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int returnVal = chooser.showOpenDialog(null); //stores user input when they click open or cancel
        if (returnVal == JFileChooser.APPROVE_OPTION) { //if the user presses open
            File userFile = chooser.getSelectedFile();//gathers the selected file
            System.out.println("Arena saved!\n" + "File Name: " + userFile.getName() + "\nDirectory: " + ((File) userFile).getAbsolutePath()); //saves the file in chosen directory
            FileWriter fw = new FileWriter(userFile);
            BufferedWriter br = new BufferedWriter(fw);
            br.write(Integer.toString(myArena.getX()));
            br.write(" ");
            br.write(Integer.toString(myArena.getY()));
            br.newLine();
            for (Drone d : myArena.droneList) {
                br.write(Integer.toString(d.getX()));
                br.write(" ");
                br.write(Integer.toString(d.getY()));
                br.write(" ");
                br.write(Integer.toString(d.getDir().ordinal()));
                br.newLine();
            }
            br.close();
        }
    }
    /**
     * This function saves the current arena that the user is simulating. it first creates a new String variable, and
     * asks the system to get the property under the name user.dir. it then creates a new File chooser variable.
     * The next line sets the file selection mode to FILES AND DIRECTORIES. It then looks at user input and then goes to printing out
     * a message about the user saving the file, it shows the file name and the directory it was saved in. It then
     * creates a new FileWriter variable and a new BufferedWriter variable, these are used to write the files, the next
     * four lines define what information should be written in the file. A for loop is then used to iterate through all
     * the drones in the list and then writes their coordinates and their direction in the file. The variable is then closed.
     * The function allows the user to set a custom name to the file they are saving.
     */

    public void loadFile() throws IOException {
        String currentDirectory = System.getProperty("user.dir");
        JFileChooser chooser = new JFileChooser(currentDirectory);
        chooser.setDialogTitle("Load arena from: ");// Window title
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);// What files are shown
        int returnVal = chooser.showOpenDialog(null); // stores if user clicks open/cancel
        if (returnVal == JFileChooser.APPROVE_OPTION) {// if user presses open
            File userFile = chooser.getSelectedFile(); // gets the file selected by use
            if (chooser.getSelectedFile().isFile()) { // if the file exists
                try {
                    System.out.println("Arena Loaded!\n" + "File Name: " + userFile.getName() + "\nDirectory: " + userFile.getAbsolutePath());// prints file chosen and directory
                    // Clear the current drone list
                    if (!myArena.droneList.isEmpty()) {
                        myArena.droneList.clear();
                    }
                    Scanner fileReader = new Scanner(userFile);
                    int xSize = fileReader.nextInt();
                    int ySize = fileReader.nextInt();
                    myArena = new DroneArena(xSize, ySize); // creates a new arena with the gathered dimensions
                    while (fileReader.hasNextInt()) { // while not in the end of the file
                        int a = fileReader.nextInt();
                        int b = fileReader.nextInt();
                        int direct = fileReader.nextInt();
                        // creates drone and adds it do list
                        myArena.droneList.add(new Drone(a, b, Direction.values()[direct]));
                    }
                    fileReader.close();
                } catch (FileNotFoundException x) {
                    System.out.println("File not found");
                    x.printStackTrace();
                }

            }
        }
    }
    /**
     * This function loads previously saved arenas. These can be of any size and any number of drones.
     * It opens up a window just like in the save function to allow the user to pick which file they want to load.
     * It then clears the current droneList to load the user selected file.
     * It then begins to create the arena with the dimensions given in the file, and adds the drones in the given
     * coordinates for each drone. It also adds their dimension and prepares the simulation to display the loaded file
     * It then closes the fileReader method. The entire code is enclosed within a try and catch statement to catch
     * FileNotFound exception incase something goes wrong.
     */



    public static void main(String[] args) throws IOException {
        DroneInterface r = new DroneInterface();
    }
    //Simply calls a new interface to the program and executes it through the main function.
}